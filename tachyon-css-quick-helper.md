# Tachyon CSS QUICK HELPER

**Reference from [tachyons.io](http://tachyons.io/) **

## TYPOGRAPHY

_To create and design an easily readable interface_

### font sizes

* 6rem (96px)
* 5rem (80px)
* 3rem (48px)
* 2.25rem (36px)
* 1.5rem (24px)
* 1.25rem (20px)
* 1rem (16px)
* .875rem (14px)
* .75rem (12px)

#### how to use

``` html
<h1 class="f1 lh-title">Title</h1>
```

#### font size classes 

``` CSS
.f1 {
    font-size: 3rem;
}

.f2 {
    font-size: 2.25rem;
}

.f3 {
    font-size: 1.5rem;
}

.f4 {
    font-size: 1.25rem;
}

.f5 {
    font-size: 1rem;
}

.f6 {
    font-size: .875rem;
}

.f7 {
    font-size: .75rem;
```

### line height classes

_line-height affects how easy it is to read a piece of text_

``` CSS
.lh-solid {
    line-height: 1;
}

.lh-title {
    line-height: 1.25;
}

.lh-copy {
    line-height: 1.5;
}
```

### letter spacing classes

_letter-spacing is the consistent white-space between letters_

``` CSS
.tracked {
    letter-spacing: .1em;
}

.tracked-tight {
    letter-spacing: -.05em;
}

.tracked-mega {
    letter-spacing: .25em;
}
```

### font weight classes

_varying the font-weight of different pieces of text can help create contrast between pieces of information_

``` CSS
.normal {
    font-weight: normal;
}

.b {
    font-weight: bold;
}

.fw1 {
    font-weight: 100;
}

.fw2 {
    font-weight: 200;
}

.fw3 {
    font-weight: 300;
}

.fw4 {
    font-weight: 400;
}

.fw5 {
    font-weight: 500;
}

.fw6 {
    font-weight: 600;
}

.fw7 {
    font-weight: 700;
}

.fw8 {
    font-weight: 800;
}

.fw9 {
    font-weight: 900;
}
```

### font style classes

_Italics can be used to emphasize a piece of content_

``` CSS
.i {
    font-style: italic;
}

.fs-normal {
    font-style: normal;
}
```

### vertical align classes

_vertical align works on inline-level elements (display inline and inline-block) and on table cells._

``` CSS
.v-base {
    vertical-align: baseline;
}

.v-mid {
    vertical-align: middle;
}

.v-top {
    vertical-align: top;
}

.v-btm {
    vertical-align: bottom;
}
```

### text align classes

_these are simple utilities for setting text-alignment to the left, right, or center of an element._

``` CSS
.tl {
    text-align: left;
}

.tr {
    text-align: right;
}

.tc {
    text-align: center;
}

.tj {
    text-align: justify;
}
```

### text transform classes

_use these classes to set the capitalization of text._

``` CSS
.ttc {
    text-transform: capitalize;
}

.ttl {
    text-transform: lowercase;
}

.ttu {
    text-transform: uppercase;
}

.ttn {
    text-transform: none;
}
```

### text decoration classes

_utilities for decorating text with underlines, or removing the default underlines browsers put on links_

``` CSS
.strike {
    text-decoration: line-through;
}

.underline {
    text-decoration: underline;
}

.no-underline {
    text-decoration: none;
}
```

### white spaces classes

_white space in css is used to control how whitespace is rendered_

``` CSS
.ws-normal {
    white-space: normal;
}

.nowrap {
    white-space: nowrap;
}

.pre {
    white-space: pre;
}
```

### font family classes

_pre-defined classes for setting the typeface of a page or element._

``` CSS
.sans-serif {
    font-family: -apple-system, BlinkMacSystemFont,

        'avenir next', avenir,
        'helvetica neue', helvetica,
        ubuntu,
        roboto, noto,
        'segoe ui', arial,
        sans-serif;

}

.serif {
    font-family: georgia,

        times,
        serif;

}

.system-sans-serif {
    font-family: sans-serif;
}

.system-serif {
    font-family: serif;
}
```

## LAYOUT

### spacing classes

_tachyons features a spacing scale based on powers of two that starts at .25rem (for most devices this will be the equivalent of 4px)._

** padding **

* pa - padding all
* pl = padding left
* pr = padding right
* pb = padding bottom
* pt = padding top
* pv = padding vertical (combination of top, bottom)
* ph = padding horizontal (combination of left, right)

``` CSS
.pa0 {
    padding: 0;
}

.pa1 {
    padding: .25rem;
}

.pa2 {
    padding: .5rem;
}

.pa3 {
    padding: 1rem;
}

.pa4 {
    padding: 2rem;
}

.pa5 {
    padding: 4rem;
}

.pa6 {
    padding: 8rem;
}

.pa7 {
    padding: 16rem;
}

.pl0 {
    padding-left: 0;
}

.pl1 {
    padding-left: .25rem;
}

.pl2 {
    padding-left: .5rem;
}

.pl3 {
    padding-left: 1rem;
}

.pl4 {
    padding-left: 2rem;
}

.pl5 {
    padding-left: 4rem;
}

.pl6 {
    padding-left: 8rem;
}

.pl7 {
    padding-left: 16rem;
}

.pr0 {
    padding-right: 0;
}

.pr1 {
    padding-right: .25rem;
}

.pr2 {
    padding-right: .5rem;
}

.pr3 {
    padding-right: 1rem;
}

.pr4 {
    padding-right: 2rem;
}

.pr5 {
    padding-right: 4rem;
}

.pr6 {
    padding-right: 8rem;
}

.pr7 {
    padding-right: 16rem;
}

.pb0 {
    padding-bottom: 0;
}

.pb1 {
    padding-bottom: .25rem;
}

.pb2 {
    padding-bottom: .5rem;
}

.pb3 {
    padding-bottom: 1rem;
}

.pb4 {
    padding-bottom: 2rem;
}

.pb5 {
    padding-bottom: 4rem;
}

.pb6 {
    padding-bottom: 8rem;
}

.pb7 {
    padding-bottom: 16rem;
}

.pt0 {
    padding-top: 0;
}

.pt1 {
    padding-top: .25rem;
}

.pt2 {
    padding-top: .5rem;
}

.pt3 {
    padding-top: 1rem;
}

.pt4 {
    padding-top: 2rem;
}

.pt5 {
    padding-top: 4rem;
}

.pt6 {
    padding-top: 8rem;
}

.pt7 {
    padding-top: 16rem;
}

.pv0 {
    padding-top: 0;
    padding-bottom: 0;
}

.pv1 {
    padding-top: .25rem;
    padding-bottom: .25rem;
}

.pv2 {
    padding-top: .5rem;
    padding-bottom: .5rem;
}

.pv3 {
    padding-top: 1rem;
    padding-bottom: 1rem;
}

.pv4 {
    padding-top: 2rem;
    padding-bottom: 2rem;
}

.pv5 {
    padding-top: 4rem;
    padding-bottom: 4rem;
}

.pv6 {
    padding-top: 8rem;
    padding-bottom: 8rem;
}

.pv7 {
    padding-top: 16rem;
    padding-bottom: 16rem;
}

.ph0 {
    padding-left: 0;
    padding-right: 0;
}

.ph1 {
    padding-left: .25rem;
    padding-right: .25rem;
}

.ph2 {
    padding-left: .5rem;
    padding-right: .5rem;
}

.ph3 {
    padding-left: 1rem;
    padding-right: 1rem;
}

.ph4 {
    padding-left: 2rem;
    padding-right: 2rem;
}

.ph5 {
    padding-left: 4rem;
    padding-right: 4rem;
}

.ph6 {
    padding-left: 8rem;
    padding-right: 8rem;
}

.ph7 {
    padding-left: 16rem;
    padding-right: 16rem;
}
```

** margin **

* ma - margin all
* ml = margin left
* mr = margin right
* mb = margin bottom
* mt = margin top
* mv = margin vertical (combination of top, bottom)
* mh = margin horizontal (combination of left, right)

``` CSS
.ma0 {
    margin: 0;
}

.ma1 {
    margin: .25rem;
}

.ma2 {
    margin: .5rem;
}

.ma3 {
    margin: 1rem;
}

.ma4 {
    margin: 2rem;
}

.ma5 {
    margin: 4rem;
}

.ma6 {
    margin: 8rem;
}

.ma7 {
    margin: 16rem;
}

.ml0 {
    margin-left: 0;
}

.ml1 {
    margin-left: .25rem;
}

.ml2 {
    margin-left: .5rem;
}

.ml3 {
    margin-left: 1rem;
}

.ml4 {
    margin-left: 2rem;
}

.ml5 {
    margin-left: 4rem;
}

.ml6 {
    margin-left: 8rem;
}

.ml7 {
    margin-left: 16rem;
}

.mr0 {
    margin-right: 0;
}

.mr1 {
    margin-right: .25rem;
}

.mr2 {
    margin-right: .5rem;
}

.mr3 {
    margin-right: 1rem;
}

.mr4 {
    margin-right: 2rem;
}

.mr5 {
    margin-right: 4rem;
}

.mr6 {
    margin-right: 8rem;
}

.mr7 {
    margin-right: 16rem;
}

.mb0 {
    margin-bottom: 0;
}

.mb1 {
    margin-bottom: .25rem;
}

.mb2 {
    margin-bottom: .5rem;
}

.mb3 {
    margin-bottom: 1rem;
}

.mb4 {
    margin-bottom: 2rem;
}

.mb5 {
    margin-bottom: 4rem;
}

.mb6 {
    margin-bottom: 8rem;
}

.mb7 {
    margin-bottom: 16rem;
}

.mt0 {
    margin-top: 0;
}

.mt1 {
    margin-top: .25rem;
}

.mt2 {
    margin-top: .5rem;
}

.mt3 {
    margin-top: 1rem;
}

.mt4 {
    margin-top: 2rem;
}

.mt5 {
    margin-top: 4rem;
}

.mt6 {
    margin-top: 8rem;
}

.mt7 {
    margin-top: 16rem;
}

.mv0 {
    margin-top: 0;
    margin-bottom: 0;
}

.mv1 {
    margin-top: .25rem;
    margin-bottom: .25rem;
}

.mv2 {
    margin-top: .5rem;
    margin-bottom: .5rem;
}

.mv3 {
    margin-top: 1rem;
    margin-bottom: 1rem;
}

.mv4 {
    margin-top: 2rem;
    margin-bottom: 2rem;
}

.mv5 {
    margin-top: 4rem;
    margin-bottom: 4rem;
}

.mv6 {
    margin-top: 8rem;
    margin-bottom: 8rem;
}

.mv7 {
    margin-top: 16rem;
    margin-bottom: 16rem;
}

.mh0 {
    margin-left: 0;
    margin-right: 0;
}

.mh1 {
    margin-left: .25rem;
    margin-right: .25rem;
}

.mh2 {
    margin-left: .5rem;
    margin-right: .5rem;
}

.mh3 {
    margin-left: 1rem;
    margin-right: 1rem;
}

.mh4 {
    margin-left: 2rem;
    margin-right: 2rem;
}

.mh5 {
    margin-left: 4rem;
    margin-right: 4rem;
}

.mh6 {
    margin-left: 8rem;
    margin-right: 8rem;
}

.mh7 {
    margin-left: 16rem;
    margin-right: 16rem;
}
```
